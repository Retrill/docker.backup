<?php
namespace Webformat\Docker;

class Backup{
    protected $basedir;
    protected $settings;
    protected $writelog;

    public function __construct(){
        $this->basedir = __DIR__.\DIRECTORY_SEPARATOR;
    }

    public function __invoke(){
        $start = microtime(true);
        $this->settings = $this->parseSettings();
        $this->init();
        $containers = $this->getContainersList();
        $originalSettings = $this->settings;
        $useStop = !empty($this->settings['backup']['stop_containers']);

        foreach($containers as $containerId => $containerName){
            $this->report("Start to backuping container $containerName (id = $containerId)", true);
            $this->settings = $this->replace([
                'container_id' => $containerId,
                'container_name' => str_replace([' '], ['_'], $containerName),
            ], $originalSettings);

            $backupCmd = $this->buildCmdTpl();

            $backupCmd = str_replace('#container_id#', $containerId, $backupCmd);

            $needToRestart = $useStop && !$this->isStopped($containerId);
            if($needToRestart){
                exec('docker stop '.$containerId, null, $stopResult);
                if($stopResult !== 0){
                    $this->report('Container stopping failured ('.$containerName.')!');
                }
            }
            shell_exec($backupCmd);
            if ($needToRestart && (0 === $stopResult)) {
                exec('docker start '.$containerId, null, $startResult);
                if($startResult !== 0){
                    $this->report('Container start failured ('.$containerName.')!');
                }
            }
            unset($startResult, $stopResult);
        }
        $this->report('Done ['.round(microtime(true) - $start).' sec]', true);
    }

    protected function init(){
        $this->writelog = !empty($this->settings['backup']['logfile']);
    }

    protected function parseSettings(){
        $generalSettingsFile = $this->basedir.'settings.ini';
        $customSettingsFile = $this->basedir.'settings.env.ini';
        if(!file_exists($generalSettingsFile)){
            throw new \Exception($generalSettingsFile.' file not found!');
        }
        if(!file_exists($customSettingsFile)){
            throw new \Exception($customSettingsFile.' file not found!');
        }
        if(!empty($this->settings['backup']['add']) && !is_array($this->settings['backup']['add'])){
            throw new \Exception('List of containers to add should be an array! Check your settings.env.ini');
        }

        $general = parse_ini_file($generalSettingsFile, true);
        $custom = parse_ini_file($customSettingsFile, true);

        $result = array_replace_recursive ($general, $custom);
        $result = $this->replace([
            'date' => date('Y-m-d'),
            'datetime' => date('Y-m-d_H-i-s'),
            'basedir' => __DIR__
        ], $result);
        if(!empty($result['backup']['logfile'])){
            $this->initdir(dirname($result['backup']['logfile']));
        }

        return $result;
    }

    protected function getContainersList(){
        $onlyActive = !empty($this->settings['backup']['only_active']);
        $cmd = 'docker ps '.($onlyActive ? '' : '-a ').'--format \'{{.ID}} = "{{.Names}}"\'';
        $containers = parse_ini_string(shell_exec($cmd), false);

        if(!empty($this->settings['backup']['add']) && !in_array('*', $this->settings['backup']['add'])){
            $containers = array_intersect_key($containers, array_flip($this->settings['backup']['add']));
        }

        if(!empty($this->settings['backup']['ignore']) && is_array($this->settings['backup']['ignore'])){
            $containers = array_diff_key($containers, array_flip($this->settings['backup']['ignore']));
        }

        return $containers;
    }

    protected function buildCmdTpl(){
        $parts = ['docker export #container_id#'];
        if($compression = $this->buildCompressionPart()){
            $parts[] = $compression;
        }
        if($encryption = $this->buildEncryptionPart()){
            $parts[] = $encryption;
        }
        $parts[] = $this->buildOutputPart();

        return implode(' | ', $parts);
    }

    protected function buildCompressionPart(){
        if (empty($this->settings['compression']['enabled'])) {
            return false;
        }
        if (!empty($this->settings['compression']['cmd'])) {
            return $this->settings['compression']['cmd'];
        }
        $level = $this->settings['compression']['level'] ?: 4;
        $engine = $this->settings['compression']['engine'] ?: 'gzip';
        if(in_array($engine, ['gzip', 'bzip2', 'brotli'])){
            $cmd = "$engine -$level -c";
        }elseif($engine == 'xz'){
            $threads = (int)($this->settings['compression']['threads'] ?: 0);
            $threads = $threads ? " --threads=$threads" : '';
            $cmd = "$engine -$level -cz".$threads;
        }

        return $cmd;
    }

    protected function buildEncryptionPart(){
        if (empty($this->settings['encryption']['enabled'])) {
            return false;
        }
        if (!empty($this->settings['encryption']['cmd'])) {
            return $this->settings['encryption']['cmd'];
        }

        $algorithm = $this->settings['encryption']['algorithm'] ?: 'aes-256-cbc';
        $password = $this->settings['encryption']['pass'] ?: '';
        $cmd = "openssl enc -e -$algorithm -k '$password'";

        return $cmd;
    }

    protected function buildOutputPart(){
        $cmd = 'php '.__DIR__.'/ftp.php';
        $engine = strtolower($this->settings['output']['engine'] ?: 'localfie');

        $filepath = $this->settings['output']['filepath'];
        if(empty($filepath)){
            throw new \Exception('Output filepath is empty! Check your settings.env.ini');
        }

        if($engine == 'localfile'){
            $this->initdir(dirname($filepath));
        }elseif($engine == 'ftp'){
            if(empty($this->settings['output']['host'])){
                throw new \Exception('FTP host cannot be empty! Check your settings.env.ini');
            }
            $host = $this->settings['output']['host'];
            $user = $this->settings['output']['user'] ?: '';
            $pass = $this->settings['output']['pass'] ?: '';

            $cmd .= " -h'$host' -u'$user' -p'$pass'";
        }else{
            throw new \Exception('Unsupported output engine "'.$engine.'"!');
        }

        $cmd .= ' -f"'.$filepath.'"';

        $partLimit = $this->settings['output']['max_file_size'] ?: '0';
        $cmd .= " -l'$partLimit'";

        if(!empty($this->settings['backup']['logfile'])){
            $cmd .= ' -L"'.$this->settings['backup']['logfile'].'"';
        }

        $cmd .= " -M50";

        return $cmd;
    }

    protected function replace($replacements, $source){
        foreach($source as $sectionName => &$section){
            foreach ($section as $paramName => &$paramValue) {
                if (!empty($paramValue) && is_scalar($paramValue)) {
                    foreach ($replacements as $search => $replace) {
                        $paramValue = str_replace("#$search#", $replace, $paramValue);
                    }
                }
            }
            unset($paramValue);
        }
        unset($section);
        return $source;
    }

    protected function initdir($dirname){
        // $dirname = dirname($filepath);
        if(is_dir($dirname)){
            return true;
        }
        if(!mkdir($dirname, 0755, true)){
            throw new \Exception('Can\'t create directory "'.$dirname.'"');
        }

        return true;
    }

    protected function log($msg){
        if (!$this->writelog) {
            return false;
        }
        file_put_contents(
            $this->settings['backup']['logfile'], 
            '['.date('Y-m-d_H:i:s').']'.str_repeat("\t", 1).$msg."\n", 
            \FILE_APPEND
        );
    }

    protected function report($msg, $writeToLog = false){
        fwrite(\STDERR, '['.date('Y-m-d H:i:s').'] '.$msg."\n");
        if($writeToLog){
            $this->log($msg);
        }
    }

    protected function isStopped($containerId){
        $all = $active = [];
        exec('docker ps -a', $all);
        exec('docker ps', $active);
        $stopped = array_diff($all, $active);

        return in_array($containerId, $stopped, true);
    }
}

$backup = new Backup();
$backup();
