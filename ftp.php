<?php
namespace Webformat\Ftp;

/*
cat ... | php ftp.php -f /home/bitrix/backup/tmp/output.txt -l 10G -r 8K

*/

Class FtpSender{
    protected function getOptions(){
        static $options;
        if(isset($options)){return $options;}
        $optCodes = array(
            'h' => 'host',
            'u' => 'user',
            'p' => 'password',
            'f' => 'filename',
            'r' => 'stdin-read-step',
            'l' => 'limit', //K, M, G
            'P' => 'pad-length',
            'L' => 'logfile', //logfile path
            'M' => 'mb-report-step' //megabytes report interval (while sending part)
        );
        $defaults = array(
            'f' => 'archive', 
            'r' => '8K', 
            'l' => 0,
            'P' => 3,
            'L' => '',
            'M' => 100
        );
        $options = getopt(
            implode(':', array_keys($optCodes)).':'
        );
        $options = $options + $defaults; //simple inheritance
        $options['l'] = $this->getCanonicalBytes($options['l']);
        $options['r'] = $this->getCanonicalBytes($options['r']);
        return $options;
    }
    protected function getCanonicalBytes($value){
        $value = str_replace(array(',', ' '), array('.', ''), ($value));
        if(!preg_match('#^([\d\.]+)([A-Za-z]+)$#', $value, $matches)){
            return (float)$value;
        }
        $measureUnit = strtoupper($matches[2]);
        $value = (float)$matches[1];

        switch($measureUnit){
            case 'G': $value *= 1024;
            case 'M': $value *= 1024;
            case 'K': $value *= 1024;
            case 'B': break;
            default: throw new \Exception('Unexpected measure unit "'.$measureUnit.'"!');
        }
        return $value;
    }
    public function send(){
        $options = $this->getOptions();
        $stdIn = fopen('php://stdin', 'rb');
        if(!isset($options['u'], $options['p'], $options['h'])){
            $remoteFilePath = $options['f'];
        }else{
            $remoteFilePath = 'ftp://'.$options['u'].':'.$options['p'].'@'.$options['h'].'/'.$options['f'];
        }
        $remoteRes = fopen($remoteFilePath, 'w');
        $overflow = NULL;
        
        $archivePart = 1;
        $this->write('Отправляем часть '.$archivePart.', "'.basename($remoteFilePath).'"', 3);
        $sentBytes = 0;
        $start = microtime(true);
        while($this->sendPart($stdIn, $remoteRes, $options['l'], $overflow, $partSize)){
        	$sentBytes += $partSize;
        	$this->write('Размер части (отправлено): '.$this->formatBytes($partSize, 'M'), 3);
            fclose($remoteRes);
            $archivePart++;
            $remotePartName = $remoteFilePath.'.'.str_pad($archivePart, $options['P'], '0', STR_PAD_LEFT);
            $remoteRes = fopen($remotePartName, 'w');
            $this->write('Отправляем часть '.$archivePart.', "'.basename($remotePartName).'"', 3);
        }
        $sentBytes += $partSize;
        $this->write('Размер части (отправлено): '.$this->formatBytes($partSize, 'M'), 3);
        $speed = $sentBytes / (microtime(true) - $start);
        
        $this->write('Всего отправлено: '.$this->formatBytes($sentBytes, 'M').", $sentBytes bytes, ".$this->formatBytes($speed, 'K').'/sec', 3);
        fclose($remoteRes);
        fclose($stdIn);
    }
    protected function sendPart($from, $to, $maxBytes = 0, &$overflow, &$dataLength){
        if(feof($from)){return false;}
        $options = $this->getOptions();
        $readLength = $options['r'];
        $dataLength = 0;
        if(isset($overflow)){
            $dataLength += mb_strlen($overflow, '8bit');
            fwrite($to, $overflow);
            $overflow = NULL;
        }
        
        if(!$maxBytes){
            while(!feof($from)){
                fwrite($to, fread($from, $readLength));
            }
        }else{
        	$lastReportStep = 0;
        	$stepStart = microtime(true);
        	$prevSent = 0;
            while(!feof($from)){
                if($dataLength >= $maxBytes){
                    if(isset($overflow)){return true;}
                    if(feof($from)){return false;}
                    //if(ftell($from) == $maxBytes){return false;}
                    if($nextByte = fread($from, 1)){
                        $overflow = $nextByte;
                    }else{return false;}
                    return true;
                }else{
                    $piece = fread($from, $readLength);
                    $pieceLength = mb_strlen($piece, '8bit');
                    if($dataLength + $pieceLength > $maxBytes){
                        $pieceLength = $maxBytes - $dataLength;
                        $split = $this->splitBytes($piece, $pieceLength);
                        $dataLength = $maxBytes;
                        $piece = $split[0];
                        $overflow = $split[1];
                    }else{
                        $dataLength += $pieceLength;
                    }
                    fwrite($to, $piece);
                }
                $curStep = floor(($dataLength / 1024 / 1024) / $options['M']);
                if($curStep > $lastReportStep){
                	$now = microtime(true);
                	$speed = round(($dataLength - $prevSent) / ($now - $stepStart));
                	$stepStart = $now;
                	$prevSent = $dataLength;
					$lastReportStep = $curStep;
					$this->write('---> отправлено: '.$this->formatBytes($dataLength, 'M').", $dataLength bytes, ".$this->formatBytes($speed, 'K'). '/sec', 4);
                }
            }
        }
        return false;
    }
    protected function splitBytes($data, $length){
        return array(
            mb_substr($data, 0, $length, '8bit'),
            mb_substr($data, $length, NULL, '8bit')
        );
    }
    
    protected function write($msg, $padNum = 0){
		$options = $this->getOptions();
		if(empty($options['L'])){return;}
		$logfile = $options['L'];
		file_put_contents($logfile, '['.date('Y-m-d_H:i:s').']'.str_repeat("\t", $padNum).$msg."\n", \FILE_APPEND);
    }
    
    public static function formatBytes($bytes, $minUnit = 'B'){
        if(!is_numeric($bytes)){return $bytes;}
        //if(strpos($bytes, '%') !== false){return $bytes;}
        $bytes = round($bytes);
        if(!$bytes){return 0;}
        $minUnit = strtoupper($minUnit);
        $units = array('B' => 1);
        $units['K'] = 1024 * $units['B'];
        $units['M'] = 1024 * $units['K'];
        $units['G'] = 1024 * $units['M'];
        $units['T'] = 1024 * $units['G'];
        $units = array_reverse($units);
        foreach($units as $notation => $weight){
            if(($bytes % $weight == 0) || ($notation == $minUnit)){
                return (round($bytes / $weight).($notation == 'B' ? '' : $notation));
            }
        }
    }
}

$sender = new FtpSender();
$sender->send();
